<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('member_id')->unsigned();
            $table->bigInteger('b_tour_order_id')->unsigned();
            $table->bigInteger('tour_order_template_id')->unsigned();
            $table->bigInteger('admin_id')->unsigned();
            $table->integer('discuss_method')->nullable();
            $table->json('discuss_time')->nullable();
            $table->date('hearing_first_time')->nullable();
            $table->text('hearing_tour_purpose');
            $table->string('hearing_budget', 191)->nullable();
            $table->string('hearing_plan_time')->nullable();
            $table->string('hearing_area')->nullable();
            $table->bigInteger('adult_count')->unsigned()->nullable();
            $table->bigInteger('child_count')->unsigned()->nullable();
            $table->text('hearing_room_allocation');
            $table->text('hearing_meal_note');
            $table->text('hearing_tour_note');
            $table->text('hearing_other_note');
            $table->text('introduce_info');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->date('application_deadline')->nullable();
            $table->integer('proposal_status')->unsigned()->nullable();
            $table->integer('invoice_status')->unsigned()->nullable();
            $table->date('payment_deadline')->nullable();
            $table->integer('payment_status')->unsigned()->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->date('payment_date')->nullable();
            $table->bigInteger('payment_confirm_admin_id')->unsigned()->nullable();
            $table->tinyInteger('is_receive_info')->unsigned()->nullable();
            $table->tinyInteger('type')->unsigned()->nullable();
            $table->text('note');
            $table->json('information_create_plan')->nullable();
            $table->json('information_verify_plan')->nullable();
            $table->json('information_draft_booking')->nullable();
            $table->json('information_booking')->nullable();
            $table->bigInteger('user_confirmed_id')->unsigned()->nullable();
            $table->tinyInteger('first_time_support_review_status')->comment('1: Reviewing, 2 finish_review')->unsigned();
            $table->tinyInteger('input_hearing_sheet_review_status')->comment('1: Reviewing, 2 finish_review')->unsigned();
            $table->tinyInteger('input_tour_plan_review_status')->comment('1: Reviewing, 2 finish_review')->unsigned();
            $table->tinyInteger('final_information_review_status')->comment('1: Reviewing, 2 finish_review')->unsigned();
            $table->tinyInteger('want_to_mail')->unsigned()->default(0);
            $table->datetime('lock_paypal_checkout')->nullable();
            $table->bigInteger('adult_count_tour')->unsigned()->nullable();
            $table->bigInteger('child_count_tour')->unsigned()->nullable();
            $table->datetime('consultation_date')->nullable();
            $table->tinyInteger('budget_remark_status')->unsigned()->default(1);
            $table->text('budget_remark');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('numbers')->onDelete('cascade');
            $table->foreign('payment_confirm_admin_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};