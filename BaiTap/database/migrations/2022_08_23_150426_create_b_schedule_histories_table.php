<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_schedule_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('b_tour_order_id')->unsigned();
            $table->bigInteger('b_tour_order_schedule_id')->unsigned();
            $table->date('schedule_date')->nullable();
            $table->tinyInteger('is_draft_reserved')->nullable();
            $table->tinyInteger('is_final_reserved')->nullable();
            $table->text('reserved_content')->nullable();
            $table->bigInteger('admin_id')->unsigned();
            $table->string('admin_name', 191);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_schedule_histories');
    }
};